drop table `mall_order` if exists;
drop table `mall_order_item` if exists;
create table `mall_order` (
    id int auto_increment,
    order_no varchar(255),
    user_id int,
    create_time timestamp,
    primary key (id)
);
create table `mall_order_item` (
    id int auto_increment,
    sku_id int,
    sku_code varchar(50),
    order_id int,
    num int,
    price int,
    create_time timestamp,
    primary key (id)
);