package com.haley.mall.order.domain.repository.persitence;

import com.haley.mall.order.domain.repository.facade.OrderRepository;
import com.haley.mall.order.domain.repository.mapper.OrderDao;
import com.haley.mall.order.domain.repository.po.OrderPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderRepositoryImpl implements OrderRepository {
    @Autowired
    private OrderDao orderDao;
    @Override
    public void insert(OrderPO orderPO) {
        orderDao.save(orderPO);
    }

    @Override
    public List<OrderPO> findByUserId(Long userId) {
       return  orderDao.findByUserId(userId);
    }
}
