package com.haley.mall.order.domain.service;

import com.google.common.collect.Lists;
import com.haley.mall.order.domain.entity.Order;
import com.haley.mall.order.domain.repository.facade.OrderRepository;
import com.haley.mall.order.domain.repository.po.OrderPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderDomainService {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderFactory orderFactory;
    public void create(Order order){
        OrderPO orderPO = orderFactory.createOrderPO(order);
        orderRepository.insert(orderPO);
    }

    public List<Order> listOrderByUserId(Long userId){

        List<OrderPO> list = orderRepository.findByUserId(userId);
        System.out.println("list size="+list.size());
        List<Order> orderList = Lists.newArrayList();
        for (OrderPO orderPO : list) {
            Order order = new Order();
            order.setCreateTime(orderPO.getCreateTime());
            order.setId(orderPO.getId());
            orderList.add(order);
        }
        return orderList;
    }
}
