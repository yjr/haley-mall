package com.haley.mall.order.interfaces.facade;

import com.haley.mall.order.application.service.OrderAppService;
import com.haley.mall.order.domain.entity.Order;
import com.haley.mall.order.infrastructure.common.api.Response;
import com.haley.mall.order.interfaces.assembler.OrderAssembler;
import com.haley.mall.order.interfaces.dto.OrderDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
@Slf4j
public class OrderApi {

    @Autowired
    private OrderAppService orderAppService;

    @PostMapping
    public Response create(@RequestBody OrderDto orderDto) {
        try {
            Order order = OrderAssembler.toDO(orderDto);
            orderAppService.create(order);
            return Response.ok();
        } catch (Exception e) {
            log.error("", e);
            return Response.failed(e.getMessage());
        }
    }

    @GetMapping
    public Response listOrder(Long userId) {
        try {
            System.out.println(userId);
            List<Order> list = orderAppService.listOrderByUserId(userId);
            return Response.ok(list);
        } catch (Exception e) {
            log.error("", e);
            return Response.failed(e.getMessage());
        }
    }
}
