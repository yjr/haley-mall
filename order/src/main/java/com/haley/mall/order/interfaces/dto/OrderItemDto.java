package com.haley.mall.order.interfaces.dto;

import lombok.Data;

@Data
public class OrderItemDto {

    private Long skuId;

    private String skuCode;

    private Long price;

    private Integer num;

}
