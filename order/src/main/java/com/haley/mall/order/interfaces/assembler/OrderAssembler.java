package com.haley.mall.order.interfaces.assembler;

import com.google.common.collect.Lists;
import com.haley.mall.order.domain.entity.Order;
import com.haley.mall.order.domain.entity.OrderItem;
import com.haley.mall.order.domain.entity.valueobj.BuyerUserVo;
import com.haley.mall.order.interfaces.dto.OrderDto;
import com.haley.mall.order.interfaces.dto.OrderItemDto;

import java.util.List;

public class OrderAssembler {
    
    public static Order toDO(OrderDto dto){
        Order order = new Order();
        List<OrderItem> itemList = Lists.newArrayList();
        List<OrderItemDto> dtoItemList = dto.getItemList();
        for (OrderItemDto orderItemDto : dtoItemList) {
            OrderItem orderItem = OrderItemAssembler.toDO(orderItemDto);
            itemList.add(orderItem);
        }
        order.setItemList(itemList);
        BuyerUserVo vo = new BuyerUserVo();
        vo.setId(dto.getUserId());
        order.setBuyer(vo);
        return order;
    }
}
