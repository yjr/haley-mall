package com.haley.mall.order.interfaces.assembler;

import com.haley.mall.order.domain.entity.OrderItem;
import com.haley.mall.order.interfaces.dto.OrderItemDto;

public class OrderItemAssembler {

    public static OrderItem toDO(OrderItemDto dto){
        OrderItem item = new OrderItem();
        item.setNum(dto.getNum());
        item.setPrice(dto.getPrice());
        item.setSkuCode(dto.getSkuCode());
        item.setSkuId(dto.getSkuId());
        return item;
    }
}
