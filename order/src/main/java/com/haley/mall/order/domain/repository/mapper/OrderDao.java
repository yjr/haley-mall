package com.haley.mall.order.domain.repository.mapper;

import com.haley.mall.order.domain.repository.po.OrderPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDao extends JpaRepository<OrderPO,Long> {

    List<OrderPO> findByUserId(Long userId);
}


