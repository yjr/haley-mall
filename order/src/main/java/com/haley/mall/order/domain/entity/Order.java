package com.haley.mall.order.domain.entity;
import com.haley.mall.order.domain.entity.valueobj.BuyerUserVo;
import com.haley.mall.order.domain.entity.valueobj.OrderStatusEnum;
import lombok.Data;
import java.util.Date;
import java.util.List;

@Data
public class Order {
    private Long id;
    private String orderNo;
    private Long money;
    private BuyerUserVo buyer;
    private OrderStatusEnum orderStatus;
    private List<OrderItem> itemList;
    private Date createTime;
    public Order create(BuyerUserVo buyer,String orderNo){
         this.setBuyer(buyer);
         this.setItemList(itemList);
         this.setCreateTime(new Date());
         this.setOrderStatus(OrderStatusEnum.CREATE);
         this.setOrderNo(orderNo);
         return this;
    }



}
