package com.haley.mall.order.domain.service;

import com.google.common.collect.Lists;
import com.haley.mall.order.domain.entity.Order;
import com.haley.mall.order.domain.entity.OrderItem;
import com.haley.mall.order.domain.repository.po.OrderItemPO;
import com.haley.mall.order.domain.repository.po.OrderPO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderFactory {

    public OrderPO createOrderPO(Order order){
        OrderPO po = new OrderPO();
        po.setOrderNo(order.getOrderNo());
        po.setUserId(order.getBuyer().getId());
        po.setCreateTime(order.getCreateTime());
        List<OrderItemPO> itemPOList = Lists.newArrayList();
        for (OrderItem orderItem : order.getItemList()) {
            OrderItemPO orderItemPO = new OrderItemPO();
            orderItemPO.setSkuId(orderItem.getSkuId());
            orderItemPO.setSkuCode(orderItem.getSkuCode());
            orderItemPO.setCreateTime(order.getCreateTime());
            orderItemPO.setNum(orderItem.getNum());
            itemPOList.add(orderItemPO);
        }
        po.setItemPOList(itemPOList);
        return po;
    }

}
