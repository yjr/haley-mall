package com.haley.mall.order.interfaces.dto;

import lombok.Data;

import java.util.List;

@Data
public class OrderDto {

    private String address;

    private Long userId;

    private List<OrderItemDto> itemList;
}
