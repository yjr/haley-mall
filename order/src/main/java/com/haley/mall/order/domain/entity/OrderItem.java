package com.haley.mall.order.domain.entity;

import lombok.Data;

@Data
public class OrderItem {
    private Long id;
    //private OrderItemVo sku;

    private Long skuId;
    private String skuCode;
    private Long price;
    private Integer num;
}
