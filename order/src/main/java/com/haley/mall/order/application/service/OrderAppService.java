package com.haley.mall.order.application.service;

import com.haley.mall.order.domain.entity.Order;
import com.haley.mall.order.domain.service.OrderDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class OrderAppService {

    @Autowired
    private OrderDomainService orderDomainService;
    public void create(Order order){
        orderDomainService.create(order);
    }

    public List<Order> listOrderByUserId(Long userId){
        return orderDomainService.listOrderByUserId(userId);
    }

}
