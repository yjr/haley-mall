package com.haley.mall.order.domain.repository.po;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mall_order_item")
@Data
public class OrderItemPO {

    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Id
    private Long id;

    private Long skuId;

    private String skuCode;

    private Long price;

    private Integer num;

    private Long orderId;

    private Date createTime;
}
