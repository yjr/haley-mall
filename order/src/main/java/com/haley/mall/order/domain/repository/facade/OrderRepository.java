package com.haley.mall.order.domain.repository.facade;

import com.haley.mall.order.domain.repository.po.OrderPO;

import java.util.List;

public interface OrderRepository {

    void insert(OrderPO orderPO);

    List<OrderPO> findByUserId(Long userId);
}
