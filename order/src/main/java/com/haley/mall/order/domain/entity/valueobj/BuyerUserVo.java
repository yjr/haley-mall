package com.haley.mall.order.domain.entity.valueobj;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BuyerUserVo {
    private Long id;
    private String name;
    private MobileVo mobile;

}
