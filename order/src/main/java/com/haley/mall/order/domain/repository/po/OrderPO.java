package com.haley.mall.order.domain.repository.po;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "mall_order")
@Data
public class OrderPO {

    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Id
    private Long id;

    private String orderNo;

    private Long userId;

    private Date createTime;

    @Transient
    private List<OrderItemPO> itemPOList;
}
